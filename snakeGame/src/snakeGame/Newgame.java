package snakeGame;

import java.awt.EventQueue;
import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Newgame {

	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Newgame window = new Newgame();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Newgame() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 570);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		frame.setLocationRelativeTo(null);
		
		JButton btnComum = new JButton("Comum");
		btnComum.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				new Main("Comum");
				frame.setVisible(false);
			}
		});
		btnComum.setBounds(237, 128, 149, 72);
		frame.getContentPane().add(btnComum);
		
		JButton btnKitty = new JButton("Kitty");
		btnKitty.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				new Main("Kitty");
				frame.setVisible(false);
			}
		});
		btnKitty.setBounds(237, 284, 149, 72);
		frame.getContentPane().add(btnKitty);
		
		JButton btnStar = new JButton("Star");
		btnStar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				new Main("Star");
				frame.setVisible(false);
			}
		});
		btnStar.setBounds(237, 434, 149, 72);
		frame.getContentPane().add(btnStar);
		frame.setVisible(true);
		
		JLabel lblNewLabel = new JLabel("");
		Image img = new ImageIcon(this.getClass().getResource("menupersonagem.png")).getImage();
		lblNewLabel.setIcon(new ImageIcon(img));
		lblNewLabel.setBounds(0, -38, 626, 604);
		frame.getContentPane().add(lblNewLabel);
		
	}
	
}
