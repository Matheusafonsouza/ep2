package snakeGame;

import java.awt.Color;
import java.awt.Graphics;
import java.util.ArrayList;
import java.util.Random;

public class Apple {

	private int Xcord, Ycord, WIDTH, HEIGHT;
	private int score;
	private String type;	
	
	
	public Apple(int Xcord, int Ycord, int tileSize) {

		this.Xcord = Xcord;
		this.Ycord = Ycord;
		this.HEIGHT = tileSize;
		this.WIDTH = tileSize;
		
		ArrayList<String> types = new ArrayList<String>();
		types.add("simple");
		types.add("big");
		types.add("decrease");
		types.add("bomb");
		
		
		Random random = new Random();
		int r = random.nextInt(4);
		
		this.type = types.get(r);
		
		
		if(type == "simple") {
			this.score = 1;
		}
		else if(type == "big") {
			this.score = 2;
		}
		else if(type == "decrease") {
			this.score = 0;
		}
		else if(type == "bomb") {
			this.score = 0;
		}
		
	}
	
	public Apple(int Xcord, int Ycord, int tileSize,String type) {

		this.Xcord = Xcord;
		this.Ycord = Ycord;
		this.HEIGHT = tileSize;
		this.WIDTH = tileSize;
		
		this.type = type;		
		
		
		if(type == "simple") {
			this.score = 1;
		}
		else if(type == "big") {
			this.score = 2;
		}
		else if(type == "decrease") {
			this.score = 0;
		}
		else if(type == "bomb") {
			this.score = 0;
		}
		else if(type == "obstaculo") {
			this.score = 0;
		}
		
	}
	
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}

	public void tick() {
		
	}
	
	public void draw(Graphics g) {
		
		if(type == "simple") {
			g.setColor(Color.RED);
		}else if(type == "bomb") {
			g.setColor(Color.YELLOW);
		}else if(type == "big") {
			g.setColor(Color.BLUE);
		}else if(type == "decrease") {
			g.setColor(Color.GREEN);
		}else if(type == "obstaculo") {
			g.setColor(Color.gray);
		}
		
		g.fillRect(Xcord * WIDTH, Ycord * HEIGHT, WIDTH, HEIGHT);
	}

	public int getXcord() {
		return Xcord;
	}

	public void setXcord(int xcord) {
		Xcord = xcord;
	}

	public int getYcord() {
		return Ycord;
	}

	public void setYcord(int ycord) {
		Ycord = ycord;
	}
	
}
