package snakeGame;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.Random;

import javax.swing.JPanel;

public class Gamepanel extends JPanel implements Runnable, KeyListener{
	
	private static final long serialVersionUID = 1L;
	
	//Tamanho da janela.
	public static final int WIDTH = 500, HEIGHT = 500;
	
	//Thread.
	private Thread thread;
	
	//Running.
	private boolean running;
	
	//Variáveis para movimentação.
	private boolean right = true,left = false,up = false, down = false;
	
		
	//variáveis da cobra.
	private Bodypart b;
	private ArrayList<Bodypart> snake;
	private int size = 5;
	private String type;
	
	//variáveis da fruta.
	private Apple apple;
	private long time;
	private ArrayList<Apple> apples;
	private ArrayList<Apple> obstaculos;
	private Random r;
	private boolean first_time = true;
	
	
	//variáveis da classe.
	private int Xcord = 10, Ycord = 10;
	private int ticks = 0;
	private int score = 0;

	public Gamepanel(String type) {
		
		setFocusable(true);
		
		setPreferredSize(new Dimension(WIDTH,HEIGHT));
		addKeyListener(this);
		
		this.snake = new ArrayList<Bodypart>();
		this.apples = new ArrayList<Apple>();
		this.obstaculos = new ArrayList<Apple>();
		
		this.r = new Random();
		this.type = type;
		start();
		
	}
	
	//Jogo começa a rodar.
	public void start() {
		running = true;
		thread = new Thread(this);
		thread.start();
		
	}
	
	public void stop() {
		running = false;
		try {
			thread.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	public void tick() {
		
		//verifica existência da cobra e gera ela no centro do mapa e com o tipo selecionado.
		if(snake.size() == 0) {
			b = new Bodypart(Xcord, Ycord, 10,type);
			snake.add(b);
		}
		
		ticks++;
		
		if(ticks > 600000) {
			if(right) Xcord++;
			if(left) Xcord--;
			if(up) Ycord--;
			if(down) Ycord++;
		
		
			ticks = 0;
		
			b = new Bodypart(Xcord, Ycord, 10,type);
			snake.add(b);
			
			if(snake.size() > this.size) {
				snake.remove(0);
			}
		}
		
		//gera localização aleatória para fruta e gera ela.
		if(apples.size() == 0) {
				
				if(first_time ==true) {
					first_time = false;
				}else {
					int counter = 0;
					
					int Xcord1 = r.nextInt(49);
					int Ycord1 = r.nextInt(49);

					int Xcord2 = r.nextInt(49);
					int Ycord2 = r.nextInt(49);
					
					
					for(int i=0;i<obstaculos.size();i++) {
						if(Xcord1 == obstaculos.get(i).getXcord() && Ycord1 == obstaculos.get(i).getYcord()) {
							counter++;
						}
						if(Xcord2 == obstaculos.get(i).getXcord() && Ycord2 == obstaculos.get(i).getYcord()) {
							counter++;
						}
						
					}
					for(int i=0;i<snake.size();i++) {
						if(Xcord2 == snake.get(i).getXcord() && Ycord2 == snake.get(i).getYcord()) {
							counter++;
						}
						if(Xcord2 == snake.get(i).getXcord() && Ycord2 == snake.get(i).getYcord()) {
							counter++;
						}
						
					}
					
					if(counter==0) {
						
						apple = new Apple(Xcord1, Ycord1, 10,"simple");
						apples.add(apple);
						
						apple = new Apple(Xcord2, Ycord2, 10);
						apples.add(apple);
						
						time = System.currentTimeMillis();
					}
				}
				
				
		}
		if(time+7000 == System.currentTimeMillis()) {
			apples.removeAll(apples);
		}
		
		
		//casos de colisão com fruta.
		for (int i=0;i<apples.size();i++) {
			if(Xcord == apples.get(i).getXcord() && Ycord == apples.get(i).getYcord()) {
				
				if(apples.get(i).getType() == "bomb") {
					//game-over
					stop();
				}
				else if(apples.get(i).getType() == "decrease") {
					//retorna para o tamanho original da cobra
					size=5;
					while(snake.size()!=5) {
						snake.remove(0);
					}
				}else if (apples.get(i).getType() == "big") {
					//ganha pontos no score e aumenta tamanho da cobra
					size +=2;
				}else{
					//ganha pontos no score e aumenta tamanho da cobra
					size ++;
				}
				
				
				//poder da cobra em questão
				if(b.getType()=="Star") {
					this.score += (apples.get(i).getScore()*2);
				}else if(b.getType()=="Comum" || b.getType()=="Kitty") {
					this.score += apples.get(i).getScore();
				}
				
				apples.removeAll(apples);
				i++;
			}
		}
		
		
		
		//casos de derrota
		for (int i=0; i< snake.size();i++) {
			//game-over cobra com cobra
			if(Xcord == snake.get(i).getXcord() && Ycord == snake.get(i).getYcord()) {
				if(i != snake.size() - 1) {
					stop();
				}
			}
		}
		
		if(b.getType()=="Star") {
			if(Xcord < 0) {
				Xcord = 49;
			}
			if(Xcord > 49) {

				Xcord = 0;
			}
			if(Ycord < 0) {

				Ycord = 49;
			}
			if(Ycord > 49) {

				Ycord = 0;
			}
		}else {
			//game-over canto da janela 
			if(Xcord < 0 || Xcord >49 ||Ycord < 0 || Ycord >49) {
				stop();
			}
		}
		
		
	}
	public void paint(Graphics g) {
		
		//cria campo da cobra.
		g.clearRect(0, 0, WIDTH, HEIGHT);
		g.setColor(Color.BLACK);
		g.fillRect(0, 0, WIDTH, HEIGHT);
		
		//cria campo quadriculado.
		for(int i=0; i<WIDTH/10;i++) {
			
			g.drawLine(i*10,0,i*10,HEIGHT);
		
		}
		//cria linhas horizontais do campo quadriculado.
		for(int i=0; i<WIDTH/10;i++) {
			
			g.drawLine(0,i*10, WIDTH, i*10);
			
		}
		
		//cria obstáculos
		for(int i =10;i <20;i++) {	
			apple = new Apple(20,i,10,"obstaculo");
			obstaculos.add(apple);
			apple = new Apple(i,40,10,"obstaculo");
			obstaculos.add(apple);
		}
		for(int i = 20;i <40;i++) {
			apple = new Apple(30,i,10,"obstaculo");
			obstaculos.add(apple);
			apple = new Apple(i,15,10,"obstaculo");
			obstaculos.add(apple);
		}
		for(int i = 0;i <10;i++) {
			apple = new Apple(10,i,10,"obstaculo");
			obstaculos.add(apple);
			apple = new Apple(i,25,10,"obstaculo");
			obstaculos.add(apple);
		}
		
		//desenha obstaculos
		for(int i=0; i<obstaculos.size();i++){
			obstaculos.get(i).draw(g);
		}
		//bater em obstaculo
		for(int i=0;i<obstaculos.size();i++) {
			if(this.type == "Kitty") {
				break;
			}
			if(Xcord == obstaculos.get(i).getXcord() && Ycord == obstaculos.get(i).getYcord()) {
				stop();
			}
		}
		
		
		//desenha cobra.
		for(int i=0; i<snake.size();i++) {
			
			snake.get(i).draw(g);
			
		}
		
		//desenha fruta.
		for(int i=0; i<apples.size();i++) {
			
			apples.get(i).draw(g);
			
		}
		
		//desenha o score
		g.setColor(Color.WHITE);
		g.setFont(new Font("arial", Font.BOLD, 14));
		g.drawString("Score: "+ score, 10, 15);	
		
		
		if(running == false) {
			//gameover
			g.clearRect(0, 0, WIDTH, HEIGHT);
			g.setColor(Color.BLACK);
			g.fillRect(0, 0, WIDTH, HEIGHT);
			g.setColor(Color.RED);
			g.setFont(new Font("arial", Font.BOLD, 30));
			g.drawString("GAME-OVER", 150, 200);	
			g.setFont(new Font("arial", Font.BOLD, 15));
			g.drawString("Your score: "+this.score, 200, 250);	
			g.dispose();
		}
		
		
	}
	
	@Override
	public void run() {
		while(running) {
			tick();
			repaint();
		}
		
	}

	@Override
	//mapeamento dos botões
	public void keyPressed(KeyEvent e) {
		
		int key = e.getKeyCode();
		if(key == KeyEvent.VK_RIGHT && !left) {
			right = true;
			up = false;
			down = false;
			left = false;
		}
		if(key == KeyEvent.VK_LEFT && !right) {
			left = true;
			up = false;
			down = false;
			right = false;
		}
		if(key == KeyEvent.VK_UP && !down) {
			up = true;
			left = false;
			right = false;
			down = false;
		}
		if(key == KeyEvent.VK_DOWN && !up) {
			down = true;
			left = false;
			right = false;
			up = false;
		}
		
		
	}

	@Override
	public void keyReleased(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyTyped(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	
}