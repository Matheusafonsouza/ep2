package snakeGame;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class Main {

	private String type;
	
	public Main(String type){
		
		JFrame frame = new JFrame();
		Gamepanel gamepanel = new Gamepanel(type);
		
		frame.add(gamepanel);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setTitle("Cobrando...");
		
		frame.pack();
		frame.setVisible(true);
		frame.setLocationRelativeTo(null);
		
	}
	
	
	
}