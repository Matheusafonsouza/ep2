package snakeGame;

import java.awt.Color;
import java.awt.Graphics;
import java.util.ArrayList;
import java.util.Random;

public class Bodypart {

	private int Xcord, Ycord, WIDTH, HEIGHT;
	private String type;
	
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Bodypart(int Xcord, int Ycord, int tileSize,String type) {
		this.Xcord = Xcord;
		this.Ycord = Ycord;
		this.HEIGHT = tileSize;
		this.WIDTH = tileSize;
		this.type = type;

	}
	
	
	public void tick() {
		
	}
	
	public void draw(Graphics g) {
		
		if(this.type == "Comum") {
			g.setColor(Color.WHITE);
		}else if(this.type == "Kitty") {
			g.setColor(Color.PINK);
		}else if(this.type == "Star") {
			g.setColor(Color.YELLOW);
		}
		
		g.fillRect(Xcord * WIDTH, Ycord * HEIGHT, WIDTH, HEIGHT);
	}

	public int getXcord() {
		return Xcord;
	}

	public void setXcord(int xcord) {
		Xcord = xcord;
	}

	public int getYcord() {
		return Ycord;
	}

	public void setYcord(int ycord) {
		Ycord = ycord;
	}
}
